#set
#immutable
# No duplicate values

set1 = {1,2,3,4,5,6,5,4,3,21}
print(set1) # {1,2,3,4,5,6,21} sequnce may change
print(type(set1)) #<class 'set'>

set2 = {'Ank',54,'sumit',32,34,87,32}
print(set2)
print(type(set2))
#set2 [2] = 'vikram'
print(set2) #typeerror does not support item assignment

