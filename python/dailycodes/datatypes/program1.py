# Numeric datatypes
# int type
stuId = 13
print(stuId)  #13  
print(type(stuId))  #<class 'int'>


jerNo = 1
print(jerNo)
print(type(jerNo))


age = 20
print(age) # 20
print(type(age)) #<class 'int'>


no_of_tickets = 2
print(no_of_tickets) #2
print(type(no_of_tickets))  #<class 'int'>

temp = -17
print(temp)  # -17
print(type(temp))  # <class 'int'>


#float type
price = 34.7
print(price) #34.7
print(type(price)) #<class 'float'>


data = 12.123456788909875543322222111678908765544333
print(data) #12.123456788909875  15 digit
print(type(data)) # <class 'float'>

height = 155.5
print(height)  #155.5cm
print(type(height))  # <class 'float'>

temp = 41.5
print(temp) #41.5
print(type(temp)) #<class 'float'>



# complex type
#a = 2+2i
#print(a) #error invalid syntax
#print(type(a))

a = 2+3j
print(a) #2+3j
print(type(a)) #<class 'complex'>

#a = 2+j
#print(a) # error name j is not defined means coeficient is nessesary.
#print(type(a))
a = 1
b = 2
c = '3'
print(a+b+c)
