#sequence datatype
#string (immutable)

print('STRING')
#way1
a = 'Ankita you are genious','you are elegant','charming girl',"fancy","""smart""",'''hardworker''', "disciplened " #Ankita you are genious','you are elegant','charming girl','fancy','smart','hardworker') 
print(a)
#way2
b = "Ankita you are beautiful girl"
print(b)
#way3
c = """Ankita you are mine""" #('Ankita you are mine')
print(c)
#way4
d = '''Ankita you are Ambitious girl'''
print(d)
print(type(a))
print(type(b))
print(type(c))
print(type(d))




print('LIST')
#list  Mutable
Teamlist = ['virat',18,31.2,'mahi',7,12.5,'k.l.rahul',1,34.6]
print(Teamlist) # ['virat',18,31.2,'mahi',7,12.5,'k.l.rahul',1,34.6]
Teamlist[6] = 'Athiyaa'
print(Teamlist) #['virat',18,31.2,'mahi',7,12.5,'Athiyaa',1,34.6]
 # duplicate data allowed
player = ['k.l.Rahul','k.l.Rahul','Athiyaa','Athiyaa']
print(player)


print('TUPLE')
#tuple immutable
friendstuple = ('Ankita',4,44.5,155.5,'diksha',29,60,165.5)
print(friendstuple) # ('Ankita',4,44.5,155.5,'diksha',29,60,165.5)
#friendstuple[0] = 'aakruti'
#print(friendstuple) #typeerror tuple does not support item assignment
# duplicate values allowed

   



print('RANGE')
#range
x = range(21)
print(x) #(0,21)
print(type(x))  # <class 'range'>
for i in x :
    print(i) #(0,1,2,3,4,5,6,7,8,9,-------,20)




y = range(10,60)
print((y))
for i in y :
    print(i)
print("End of loop")


y = range(1,60,+5)
print(y)
for i in y :
    print(i) #(1,6,11,16,21----------56)

    
A = range(100,1,-10) #(100,90,80----------------10)
for i in A :
    print (i);
