#dictionary
#Mapping
#dict
player = {7:'msdhoni',1:'k.l.rahul',18: 'virat'}
print(player) #  {7:'msdhoni',1:'k.l.rahul',18: 'virat'}
print(type(player)) #<class 'dict'>
player[18] = 'viruu'
print(player) # {7:'msdhoni',1:'k.l.rahul',18: 'viruu'} 

team = {'csk': 'chennai', 'MI': 'Mumbai'}
print(team)  #{'csk': 'chennai', 'MI': 'Mumbai'}
