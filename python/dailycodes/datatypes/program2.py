# Boolean type
a = True 
print(a) # True
print(type(a)) # <class 'bool'>

b = False
print(b)  # False
print(type(b))  #<class 'bool'>

data1 = True 
data2 = False 

Ans1 = data1 + data2
print(Ans1)  # 1

Ans2 = data2 - data1
print(Ans2) # -1

Ans3 = data1 * data2   
print(Ans3); #0
print(bool(Ans3))  # false

#Ans4 = data1 / data2
#print(Ans4)  # zero div error
#print(bool(Ans4))  # 

A = 0
print(bool(A))  # false

B = {} 
print(bool(B))  # False set

C = ()    
print(bool(C))  # False tuple /range

D = []  
print(bool(D))  # False list

F = 32
print(bool(F))  # True

G = None
print(bool(G))

# return False as x is not equal to y
x = 4
y = 29
print(bool(x == y))  #False

# return True as a is equal to b
a = 2
b = 2
print(bool(a == b)) #True

P = ''
print(bool(P))
