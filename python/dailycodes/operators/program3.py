# Relational operator
x = 20
y = 12
print(x > y)  #true
print(x < y)  #false
print(x >= y)  #true
print(x <= y)  #false
print(x != y)  #true
