#bitwise operator(&)

x = 10  # 0 0 0 0 1 0 1 0 
y = 5  # 0 0 0 0  0 1 0 1

print(x & y)  # 0 0 0 0 0 0 0 0  0


a = 4  # 0 0 0 0 1 0 0 0
b = 9  # 0 0 0 1 0 0 0 1

print(a & b)  # 0 0 0 0 0 0 0 0    0


p =  13  # 0 0 0 0 1 1 0 1
q = 15   # 0 0 0 0 1 1 1 1

print(p & q)  # 0 0 0 0 1 1 0 1  13



# bitwise OR operator (|)

x = 7   # 0 0 0 0 0 1 1 1
y = 3   # 0 0 0 0 0 0 1 1

print(x | y)  # 0 0 0 0 0 1 1 1  7

a = 1  # 0 0 0 0 0 0 0 1
b = 2  # 0 0 0 0 0 0 1 0

print(a | b)  # 0 0 0 0 0 0 1 1  3

#bitwise XOR operator (^)
a = 8 # 1 0 0 0
b = 6  # 0 1 1 0
print(a ^ b)  # 1 1 1 0  14

# bitwise left shift
a = 15   # 0 0 0 0 1 1 1 1
print (a<<3)  # 0 1 1 1 1 0 0 0  120


# bitwise right shift operator
a = 14  # 0 0 0 0 1 1 1 0
print(a>>2)  # 0 0 0 0 0 0 1 1  3


# bitwise not operator (~)
a = 4
print(~a)  #-5
