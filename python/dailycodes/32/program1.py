# Multiple inheritance
class boss(object):
    def Report(self):
        print("Boss:hey!! I am Your BOSS")
class maneger1(boss):
    def Report(self):
        print("Manager1: submit report to Boss")
class maneger2(boss):
    def Report(self):
        print("Manager2: submit report to Boss")
class developer(maneger2,maneger1):
    """def Report(self):
        print("developer: submit report to manager1 & manager2")"""
obj = developer()
obj.Report()
print(developer.mro())
print(obj)   #address
print(type(obj))  #developer
print(type(boss))  #type
print(boss.__base__)   #object

