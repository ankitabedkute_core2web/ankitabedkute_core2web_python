# Methods 11
#print("1: append()")
import array as arr
data = arr.array('i',[10,20,30,40,50])
data.append(60) #('i',[10,20,30,40,50,60])
print(data)

# 2: buffer_info()
print(data.buffer_info())

# 3: count()
print(data.count(40))

#4: extend()
data.extend((70,80,90))
print(data)





