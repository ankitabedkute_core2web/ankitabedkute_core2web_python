# fromlist

import array as arr

listdata = [10,20,100,30,40]

arrdata = arr.array('i',[100,200,300,400])
print(arrdata)

arrdata.fromlist(listdata)
print(arrdata)  #array('i',[100,200,300,400,10,20,30,40])

# index()
print(arrdata.index(400))

# inseert()
arrdata.insert(4,500)
print(arrdata)

# pop()
arrdata.pop()
print(arrdata)

#remove()
arrdata.remove(100)
print(arrdata)

#reverse
arrdata.reverse()
print(arrdata)

# tolist
print(arrdata)
copylist = arrdata.tolist()
print(copylist)
