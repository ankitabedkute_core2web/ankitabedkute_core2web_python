# constructor
class parent:
    def __init__(self):
        print("In constructor")   
        self.x = 10
        self.y = 20
    def display(self):
        print(self.x)
        print(self.y)
        x = 300
        print(x)

#obj = parent()
#obj.display()
class child(parent):
    def __init__(self):
        parent.__init__(self)
        super().__init__()
        print("In child constructor")
        self.x = 60
        self.y = 30
    def childfunc(self):
        print("In child function")
    
    #parent()
    

obj = child()
obj.display()
obj.childfunc()

