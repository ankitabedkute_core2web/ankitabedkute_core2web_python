# decorator chaining
def decorfun(x):
    print("In decorfun")
    def wrapper1():
        print("In wrapper1 function")
    def wrapper2():
        print("in wrapper2 function")
    return wrapper1, wrapper2    
def normfun():
    print("ohh its a normal function")
normfun = decorfun(normfun) 
print(normfun)

