from abc import *
class shape:
    @abstractmethod
    def area(self):
        pass
class circle(shape):
    def __init__(self,x):
        self.x = x
    def area(self):
        return f"area is{3.14 * self.x * self.x}"
circleobj = circle(10)  
print(circleobj.area())

