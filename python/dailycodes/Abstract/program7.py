from abc import *
class Animal:
    @abstractmethod
    @classmethod
    def sound(cls):
        pass
class cat(Animal):
     @classmethod
     def sound(cls):
        return "meow"
class dog(Animal):
     @classmethod
     def sound(cls):
         return "bhoo"
print("sound:", cat.sound())     
