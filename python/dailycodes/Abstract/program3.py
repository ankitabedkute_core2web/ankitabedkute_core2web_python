# Abstract class
from abc import ABC, abstractmethod
class Hyundai(ABC):
    def slogan(self):
        print("New Thinking, New beginning")
    @abstractmethod
    def cartype(self):
        pass
class creta(Hyundai):
   def cartype(self):
        print("fetchback")
"""class verna(Hyundai):
    def cartype(self):
        print("sedan")
obj1 = creta()
obj1.cartype()
obj1.slogan()
obj2 = verna()
obj2.cartype()
obj2.slogan()
print(verna.mro())
print(type(verna))"""
