'''
$# $# $#
$# $# $# 
$# $# $#
'''
for i in range(3):
    for j in range(3):
        print("$#", end = " ")
    print() 
print()
'''
1 2 3
1 2 3
1 2 3
'''
for i in range(3):
    num = 1
    for j in range(3):
        print(num, end = " ")
        num += 1
    print() 
print()
'''
1 2 3 4
1 2 3 4
1 2 3 4
1 2 3 4
'''

num = 1
for i in range(4):
    for j in range(4):
        print(num, end = " ")
    num += 1
    print() 
print() 

'''
1 1 1
2 2 2
3 3 3
'''
num = 1
for i in range(3):
    for j in range(3):
        print(num, end = " ")
    num += 1
    print()
print()

'''
1 1 1 1
2 2 2 2
3 3 3 3
4 4 4 4
'''
num = 1
for i in range(4):
    for j in range(4):
        print(num, end = " ")
    num += 1
    print()
print()


'''
A B C 
D E F
G H I
J K L
'''
num = 65
for i in range(4):
    for j in range(3):
        print(chr(num), end = " ")
        num += 1
    print() 
print() 

'''
A C E
G I K
M O Q
'''
num = 65
for i in range(3):
    for j in range(3):
        print(chr(num), end = " ")
        num += 2
    print()
print()

'''
1A 1B 1C 
1D 1E 1F
1G 1H 1I
'''
num = 65
x = '1'
for i in range(3):
    for j in range(3):
        print(x+chr(num), end = " ")
        num += 1
    print()
print()




'''
1A 1A 1A
1A 1A 1A 
1A 1A 1A
'''

num = '1'
char = 65
for i in range(3):
    for j in range(3):
        print(num + chr(char), end = " ")
    print() 
print() 

'''
1A 1A 1A 1A
1A 1A 1A 1A
1A 1A 1A 1A
1A 1A 1A 1A
'''

num = 1
char = 65
for i in range(4):
    for j in range(4):
        print(str(num) + chr(char), end = " ")
    print()
print()

'''
1A 2B 3C
4D 5E 6F
7G 8H 9I
'''
num = 65
digit = 1
for i in range(3):
    for j in range(3):
        print(str(digit)+chr(num), end = " ")
        num += 1
        digit += 1
    print() 
print()    


'''
1A 2B 3C
1A 2B 3C
1A 2B 3C
'''
for i in range(3):
    num = 65
    digit = 1
    for j in range(3):
        print(str(digit)+chr(num), end = " ")
        num += 1
        digit += 1
    print()
print() 

'''
d c b a
d c b a
d c b a
d c b a
'''
for i in range(4):
    num = 100
    for j in range(4):
        print(chr(num), end = " ")
        num -= 1
    print() 
print() 

'''
C1 C2 C3
C4 C5 C6
C7 C8 C9
'''
num = 67
digit = 1
for i in range(3):
    for j in range(3):
        print(chr(num) + str(digit), end = " ")
        digit += 1
    print() 
print() 


'''
D1 D2 D3 D4 
D5 D6 D7 D8
D9 D10 D11 D12
D13 D14 D15 D16
'''
num = 68
digit = 1
for i in range(4):
    for j in range(4):
        print(chr(num) + str(digit), end = " ")
        digit += 1
    print()    
print()

'''
1 2 3 4 
4 5 6 7
7 8 9 10
10 11 12 13
'''
num = 1
for i in range(4):
    for j in range(4):
        print(num, end = " ")
        num += 1
    print() 
    num -= 1
print() 

'''
1 2 3 
3 4 5
3 4 5
'''
num = 1
for i in range(3):
    for j in range(3):
        print(num, end = " ")
        num += 1
    print() 
    num -= 1
print()


'''
1 2 3 4
2 3 4 5
3 4 5 6
4 5 6 7
'''
num = 1
for i in range(4):
    for j in range(4):
        print(num, end = " ")
        num = num + 1
    print() 
    num -= 3
print()
    
'''
1 2 3
2 3 4
3 4 5
'''
num = 1
for i in range(3):
    for j in range(3):
        print(num, end = " ")
        num += 1
    print() 
    num -= 2





