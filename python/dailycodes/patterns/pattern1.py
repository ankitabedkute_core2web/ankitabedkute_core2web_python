'''
print * = given no in 1 line   
'''
x = int(input("Enter no. : "))
for i in range(x):
    print("*", end = " ")
print()


'''
*
*
*
print * given no times in 1 line vertically
'''
x = int(input("Enter no. : "))
for i in range(x):
    print("*")

'''
* * *
* * *
* * *
'''
x = int(input("Enter num: "))  # 3
for i in range(x):
    for j in range(x):
        print("*", end = " ")
    print()

'''
*
* *
* * *
'''

x = int(input("Enter num: "))  # 3
for i in range(x):
    for j in range(i+1):
        print("*", end = " ")
    print()


