# Triangle Pattern

"""
*
* *
* * *
* * * *
"""
for i in range(4):
    for j in range(i+1):
        print("*", end = " ")
    print()
print() 

"""
a
$ $
a b c
"""
for i in range(3):
    num = 97
    for j in range(i+1):
        if(i % 2 == 1):
            print("$", end = " ")
        else:
            print(chr(num), end =" ")
            num += 1
    print() 
print()

"""
d
D C 
d c b 
D C B A 
"""
for i in range(4):
    num = 100
    #char = chr(num)
    for j in range(i+1):
        char = chr(num)
        if(i % 2 == 1):
            print(char.upper(), end = " ")
        else:
            print(char, end =" ")
        num = num - 1
    print()
print()

"""
D 
E F
G H I
"""
num = int(input("Enter value for char: ")) # 67
for i in range(3):
    
    for j in range(i+1):
        print(chr(num), end = " ")
        num += 1
    print() 
print()    
 




