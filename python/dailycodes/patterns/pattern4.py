"""
1 
2 3
3 4 5 
"""
for i in range(3):
    num = i + 1
    for j in range(i+1):
        print(num, end = " ")
        num += 1
    print() 
print() 


"""
A
B C
C D E
"""
for i in range(3):
    num = 65 + i
    for j in range(i+1):
        print(chr(num), end = " ")
        num += 1
    print() 
print() 

"""
3
3 6 
3 6 9
"""
for i in range(3):
    num = 3
    for j in range(i+1):
        a = j+1
        print(num*a, end = " ")
    print() 
print()    
       

"""
4 
4 8
4 8 12
4 8 12 16
"""
for i in range(4):
    num = 4
    for j in range(i+1):
        print(num, end = " ")
        num += 4
    print() 
print() 

"""
1
2 4
3 6 9
"""
for i in range(3):
    num = i+1
    for j in range(i+1):
        a = j + 1
        print(num*a, end = " ")
    print()
print()

"""
1 
2 4 
3 6 9 
4 8 12 16
"""
for i in range(4):
    num = i+1
    for j in range(i+1):
        a = j+1
        print(num*a, end = "  " )
    print() 
print()



