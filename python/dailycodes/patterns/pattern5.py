# reverse triangle

"""
* * * *
* * *
* *
*
"""
for i in range(4):
    #a = 2*2-i
    for j in range(4-i):
        print("*", end = " ")
    print() 
print()

"""
3 3 3
2 2
1
"""
num = 3
for i in range(3):
    for j in range(3-i):
        print(num, end = " ")
    num -= 1
    print() 
print()

"""
4 4 4 4
3 3 3 
2 2 
1 
"""
num = 4
for i in range(4):
    for j in range(4-i):
        print(num, end = " ")
    num -= 1
    print()
print()


"""
1 2 3 4
1 2 3
1 2 
1
"""

for i in range(4):
    num = 1
    for j in range(4-i):
        print(num, end = " ")
        num += 1
    print() 
print() 

"""
1 2 3 4
2 3 4
3 4 
4
"""
for i in range(4):
    num = i+1
    for j in range(4-i):
        print(num, end = " ")
        num += 1
    print()
print()
 

"""
E D C B A
E D C B
E D C
E D
E
"""

for i in range(5):
    num = 69
    for j in range(5-i):
        print(chr(num), end = " ")
        num -= 1
    print()
print()   

"""
65 B 67 D
B  67 D
67 D
D
"""
for i in range(4):
    num = 65+i
    for j in  range(4-i):
        if(num%2 == 0):
            print(chr(num), end = " ")
        else:
            print(num, end =" ")
        num += 1    
    print() 
print()        


