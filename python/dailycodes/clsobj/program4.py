class Employee:
    #def __new__(self):
    #    print("In new ")
    def __init__(self):
        self.x = 20
        print("In constructor")
        print(type(self))
        print(id(self))
    def show(self):
        print(self.x)
        print(type(self))
        print(id(self))
obj = Employee() 
obj.__init__()
print(type(obj))
obj.show()
print(id(obj))
print(type(Employee))
print(Employee.__base__())
