#list
player =list()
print(player)

player = ['rohit', 'shubman', 'rishab', 'dhoni']
cricketer = player.copy()
print(player)
print(cricketer)

player.append("klrahul")
print(player)
print(cricketer)  # by default eth deep copy aste so change.

print("nested list")
player = list()
print(type(player))

player = ['rohit', 'shubman', 'rishab', 'dhoni',['klrahul','miller','watson']]
cricketer = player.copy()
print(player)
print(cricketer)

player.append('hardik')
print(player)
print(cricketer)


player[4][1] = 'Krunal'
print(player)
print(cricketer)   # change shallow copy

import copy as cp
player = ['rohit', 'shubman', 'rishab', 'dhoni',['klrahul','miller','watson']]
newlist = cp.deepcopy(player)
print(player)
print(newlist)

player[4][1] = "shreyas"
print(player)
print(newlist)

