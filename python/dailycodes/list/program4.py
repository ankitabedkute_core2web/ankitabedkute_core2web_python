#copy
player = ['rohit', 'shubman', 'rishab', 'shreyas', 'hardik', 'klrahul', 'dhoni']
cricketer = player.copy()
print(player)
print(cricketer)
print(player is cricketer)

#nested list
#player = ['rohit', 'shubman', 'rishab', ['shreyas', 'hardik', 'klrahul'], 'dhoni']

#print(player[3][2])
lang = ["c","cpp","java","python",["rust","go","dart","rubi"]]
print(lang)
newlist = lang.copy()
print(newlist)
print(lang is newlist)

lang.append("fluter")
print(lang)
print(newlist)

lang.insert(2,"fluter")
print(lang)
print(newlist)

lang[4][2] = 'js'
print(lang)
print(newlist)

