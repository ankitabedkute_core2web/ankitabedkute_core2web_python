#accessing elements from list
# index
friends = ["Ankita",("khushi","Ragini","Diksha"),"Teju"]
print(friends[0])
print(friends[1][2])
#print(friends[2])
print(friends[1][0])
print(friends[1][1])

#slicing
boys =("rohit","shubman","rishab","shreyas","hardik","klrahul")
player = list(boys)
print(player)
print(player[:4:2]) # rohit rishab 
print(player[::3]) #rohit shreyas
print(player[:5:3]) # rohit shreyas
print(player[4:2:2]) #[]
print(player[4:2:-1]) #hardik shreyas
print(player[-1::-1]) #reverse till shubman
print(player[::]) # in sequence all
print(player[:-6:-2]) #klrahul shreyas shubman
print(player[::-3]) #klrahul rishab
print(player[:-5:-2])# klrahul shreyas 


