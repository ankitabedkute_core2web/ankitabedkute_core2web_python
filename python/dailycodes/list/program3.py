#methods
player = ['rohit', 'shubman', 'rishab', 'shreyas', 'hardik', 'klrahul']

player.append("dhoni")
print(player)

(player.insert(2,"sanju")) 
print(player)

player.extend(("krunal","eshan kishan"))
print(player)

player.pop(7)
print(player)

player.remove('sanju')
print(player)

print(player.count('shubman'))

print(player.index('shreyas'))


player.reverse()
print(player)

player.sort()
print(player)

cricketer = player.copy()
print(cricketer)
print(player)

print(player.count("shreyas"))


