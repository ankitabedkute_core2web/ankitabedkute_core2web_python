#create list
#1. list literals
player = ["Rohit","shubman","klrahul","virat","shreyas"]
print(player)

player = ["Rohit","shubman",10,45,1,{"klrahul"},("virat","shreyas")]
print(player)
print(type(player[5])) #set
print(type(player[6])) #tuple

#2 constructor

crickTeam = ("RCB","CSK","MI","LSG","SRH","KKR","RR")

print(crickTeam)
print(type(crickTeam))

list1 = list(crickTeam)
print(list1)
print(type(list1))

#list2 = list(10,50)
#print(list2) # type error

dict ={7:"Mahi"} 
print(dict)
list3 = list(dict)
print(list3)

#set ={{18:"virat"},{45:"rohit"},{1:"klrahul"},{7:"mahi"}}
#print(set)

#list4 = list(set) #error

#comprehension
normlist = [num for num in range(1,11)]
print(normlist)

#sqr 
sqr = [num*num for num in range(1,20)]
print(sqr)
print(type(sqr))

#even
evenlist = [num for num in range(1,21) if num % 2 == 0]
print(evenlist)




