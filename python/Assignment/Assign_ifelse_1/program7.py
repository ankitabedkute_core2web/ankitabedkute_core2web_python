#WAP to print num of months and print num of days in that month

num = int (input("Enter num : "))

if (num == 1):
    print("January is 31-day month")
elif(num == 2):
    print("February is 28-day month")
elif(num == 3):
    print("March is 31-day month") 
elif(num == 4):
    print("April is 30-day month")    
elif(num == 5):
    print("May is 31-day month")
elif(num == 6):
    print("June is 30-day month")
elif(num == 7):
    print("July is 30-day month")  
elif(num == 8):             
    print("August is 31-day month")
elif(num == 9):
    print("September is 30-day month") 
elif(num == 10):
    print("october is 31-day month") 
elif(num == 11):
    print("November is 30-day month") 
elif(num == 12):
    print("December is 31-day month")     
else:
    print("Invalid Month")
