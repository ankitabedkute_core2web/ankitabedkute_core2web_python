def outer():
    def inner():
        return "This is inner function"
    return inner
if __name__ == "__main__":
    retobj = outer()
    retInner = retobj()
    print(retInner) # This is inner fn

