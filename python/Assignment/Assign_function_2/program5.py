def outer():
    def inner(outer):
        print(outer) # address of outer
        return inner
    

    return inner(outer)
if __name__ == "__main__":
    retobj = outer()    
    print(retobj)  # address of inner fn


